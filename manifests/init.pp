# == Class: cron_puppet
#
# Create a crontab entry and corresponding git post-merge hook to update
# /etc/puppet from origin every 30 minutes and apply changes if there are any.
#
# === Parameters
#
# [*user*]
#   Specify the user whose crontab should be updated.
#   Defaults to 'root'.
#
# [*email*]
#   Specify the e-mail address to which cron output should be sent.
#   Defaults to undefined.
#
# [*logfile*]
#   Specify the file for puppet to log to.
#   Defaults to /var/log/puppet/puppet.log
#
# [*code_dir*]
#   Specify the puppet code directory.
#   Defaults to /etc/puppet
#
# [*puppet_dir*}
#   Specify the puppet directory.
#   Defaults to /etc/puppet
#
# === Examples
#
#  class { 'cron_puppet': }
#    Use the default settings.
#
#  class { 'cron_puppet':
#    user  => 'foo',
#    email => 'foo@bar',
#  }
#
# === Authors
#
# Patrick Aikens <paikens@gmail.com>
#
# === Copyright
#
# Copyright 2015 Patrick Aikens
#
class cron_puppet (
  $user       = 'root',
  $email      = undef,
  $logfile    = '/var/log/puppet/puppet.log',
  $code_dir   = '/etc/puppet',
  $puppet_dir = '/etc/puppet',
)
{
  $rootgroup = $osfamily ? {
    /(Darwin|FreeBSD)/ => 'wheel',
    default            => 'root',
  }

  file { 'post-hook':
    ensure  => file,
    path    => "${puppet_dir}/.git/hooks/post-merge",
    content => template('cron_puppet/post-merge.erb'),
    mode    => '0755',
    owner   => root,
    group   => $rootgroup,
  }

  if $email {
    $environment = "MAILTO=${email}"
  } else {
    $environment = undef
  }

  cron { 'puppet-apply':
    ensure      => present,
    command     => "cd ${puppet_dir} ; /usr/bin/git pull > /dev/null",
    user        => $user,
    minute      => '*/30',
    require     => File['post-hook'],
    environment => $environment,
  }
}

