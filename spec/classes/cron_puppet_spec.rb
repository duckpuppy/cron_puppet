require "spec_helper"

describe 'cron_puppet' do
  let(:params) { { } }

  it { is_expected.to compile }
  it { should contain_file('post-hook')
    .with(
      'ensure' => 'file',
      'path' => '/etc/puppet/.git/hooks/post-merge',
      'mode' => '0755',
      'owner' => 'root',
      'group' => 'root',
    )
  }
  it { should contain_cron('puppet-apply')
    .with_ensure('present')
    .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
    .with_minute('*/30')
  }

  it { should contain_cron('puppet-apply').that_requires("File[post-hook]") }

  context 'with default parameters' do
    it {
      is_expected.to contain_cron('puppet-apply')
        .with_user('root')
        .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
        .without_environment()
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/var\/log\/puppet\/puppet\.log/)
        .with_content(/PUPPET_DIR=\/etc\/puppet/)
        .with_content(/CODE_DIR=\/etc\/puppet/)
    }
  end

  context 'with email parameter' do
    let(:params) { { :email => 'foo@bar' } }
    it { is_expected.to contain_cron('puppet-apply')
      .with_user('root')
      .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
      .with_environment("MAILTO=foo@bar")
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/var\/log\/puppet\/puppet\.log/)
    }
  end

  context 'with user parameter' do
    let(:params) { { :user => 'foo' } }
    it { is_expected.to contain_cron('puppet-apply')
      .with_user("foo")
      .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
      .without_environment()
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/var\/log\/puppet\/puppet\.log/)
    }
  end

  context 'with logfile parameter' do
    let(:params) { { :logfile => '/log/foo.log' } }
    it { is_expected.to contain_cron('puppet-apply')
      .with_user("root")
      .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
      .without_environment()
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/log\/foo\.log/)
    }
  end

  context 'with puppet_dir parameter' do
    let(:params) { { :puppet_dir => '/foo/puppet' } }
    it { is_expected.to contain_cron('puppet-apply')
      .with_user("root")
      .with_command('cd /foo/puppet ; /usr/bin/git pull > /dev/null')
      .without_environment()
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/var\/log\/puppet\/puppet\.log/)
        .with_content(/PUPPET_DIR=\/foo\/puppet/)
    }
  end

  context 'with code_dir parameter' do
    let(:params) { { :code_dir => '/foo/puppet/code' } }
    it { is_expected.to contain_cron('puppet-apply')
      .with_user("root")
      .with_command('cd /etc/puppet ; /usr/bin/git pull > /dev/null')
      .without_environment()
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/var\/log\/puppet\/puppet\.log/)
        .with_content(/CODE_DIR=\/foo\/puppet\/code/)
    }
  end
  context 'with all parameters' do
    let(:params) { 
      { 
        :logfile => '/log/foo.log',
        :user => 'foo',
        :email => 'foo@bar',
        :puppet_dir => '/foo/puppet',
        :code_dir => '/foo/puppet/code',
      } 
    }

    it { is_expected.to contain_cron('puppet-apply')
      .with_user("foo")
      .with_command('cd /foo/puppet ; /usr/bin/git pull > /dev/null')
      .with_environment("MAILTO=foo@bar")
    }
    it {
      is_expected.to contain_file('post-hook')
        .with_content(/\/log\/foo\.log/)
        .with_content(/PUPPET_DIR=\/foo\/puppet/)
        .with_content(/CODE_DIR=\/foo\/puppet\/code/)
    }
  end
end
